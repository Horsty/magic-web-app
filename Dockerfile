FROM node:12.14.1

RUN mkdir -p /src/app

RUN npm install -g @ionic/cli 

RUN cd /src/app \
    && npm install native-run cordova-res \
    && ionic start magic-search tabs --type=angular --capacitor \
    && cd magic-search \
    && ls -la \
    && npm install @ionic/pwa-elements

WORKDIR /src/app/magic-search

EXPOSE 8100 35729

ENTRYPOINT ["ionic"]
CMD ["serve", "8100", "--external" ]
